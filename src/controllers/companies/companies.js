//definir funciones para http requests
const companies = require('../../../data')
const fs = require('fs');

const controllers = {
    index: (req, res) => {
        // res.send('GET /api/v1/companies')
        res.status(200).json({data: companies})
    },
    find: (req, res) => {
        const {id} = req.params
        const found = companies.data.filter(company => company.id == id)
        res.status(200).json({data: found})
    },
    create: (req, res) => {
        const {id} = req.body
        const existing = companies.data.some(company => company.id == id)

        if(existing) {
            res.json({message: 'Company already exists', data: req.body})
        }else {
            var addedCompanies = [...companies.data, req.body];
            var companiesToWrite = JSON.stringify({data: addedCompanies});

            fs.writeFile('./data.json', companiesToWrite, (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
            })
            res.status(201).json({data: addedCompanies});
        }
    },
    delete: (req, res) => {
        const {id} = req.params
        const existing = companies.data.some(company => company.id == id)

        if(existing) {
            var newCompanies = companies.data.filter(company => company.id != id);
            var companiesToWrite = JSON.stringify({data: newCompanies});

            fs.writeFile('./data.json', companiesToWrite, (err) => {
                if (err) throw err;
                console.log('The file has been saved!')
            })
            res.json({data: newCompanies});
        }else {
            res.status(204).json({message: 'Company not found'});
        }
    },
    update: (req, res) => {
        let info = req.body

        const cid = info.id
        let found = companies.data.filter(company => company.id == cid)

        found[0] = req.body
        console.log(found)
        let newObject = found[0]
        // console.log(newObject)
        const newCompanies = companies.data.filter(company => company.id != cid)
        let newCompaniesUpdated = [...newCompanies, found[0]]
        console.log(newCompaniesUpdated)
        res.status(200).json({data: newCompaniesUpdated})   
    }
}

//exportar
module.exports = controllers;